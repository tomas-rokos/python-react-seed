from jsbridge.pyreact import Component


class Index(Component):
    def render(self):
        return self.sprite(
            ('div', {'className': 'maindiv'}, ['this is ', 'INDEX'])
        )
