from jsbridge.pyreact import Component


class Contact(Component):
    def render(self):
        return self.sprite(
            ('div', {'className': 'maindiv'}, ['CONTACT', ' in place'])
        )
