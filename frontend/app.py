from jsbridge.bootstrap import bootstrap
from jsbridge.pyreact import Component
from jsbridge.router import Router
from jsbridge.route import Route

from frontend.components.hello import Hello

from frontend.routes.index import Index
from frontend.routes.contact import Contact


class App(Component):
    def render(self):
        return self.sprite(
            ('div', None, [
                (Hello, {'name': 'React-ed'}),
                (Route, {'href': '/'}, 'index'),
                ' ',
                (Route, {'href': '/contact'}, 'contact subpage'),
                (Router, {
                    '': Index,
                    '/contact': Contact
                }),
            ])
        )


bootstrap(App)
