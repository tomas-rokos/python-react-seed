import jsbridge.testing

from frontend.app import App
from jsbridge.router import Router
from jsbridge.route import Route

from frontend.components.hello import Hello

from frontend.routes.index import Index
from frontend.routes.contact import Contact


def test_basic():
    h = App({'name': 'React'})
    assert h.render() == (
        ('div', None, [
            (Hello, {'name': 'React-ed'}),
            (Route, {'href': '/'}, 'index'),
            ' ',
            (Route, {'href': '/contact'}, 'contact subpage'),
            (Router, {
                '': Index,
                '/contact': Contact
            }),
        ])
    )
