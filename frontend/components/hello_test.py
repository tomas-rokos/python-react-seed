import jsbridge.testing

import frontend.components.hello as hello


def test_no_state():
    h = hello.Hello({'name': 'React'})
    assert h.render() is None


def test_basic():
    h = hello.Hello({'name': 'React'})
    h.componentDidMount()
    assert h.render() == \
           ('div', {'className': 'maindiv'}, [
               ('h1', None, ['Hello ', 'React']),
               ('p', None, 'Lorem ipsum dolor sit ame.'),
               ('p', {'onClick': h.on_counter},
                ['Counter: ', 3])
           ])


def test_click():
    h = hello.Hello({'name': 'React'})
    h.componentDidMount()
    h.on_counter()
    assert h.render() == \
           ('div', {'className': 'maindiv'}, [
               ('h1', None, ['Hello ', 'React']),
               ('p', None, 'Lorem ipsum dolor sit ame.'),
               ('p', {'onClick': h.on_counter},
                ['Counter: ', 4])
           ])
