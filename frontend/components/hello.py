from jsbridge.pyreact import Component


class Hello(Component):
    def __init__(self, props):
        super().__init__(props)
        print('__init__', props, self.state)

    def componentDidMount(self):
        self.setState({'counter': 3})
        print('did mount')

    def componentWillUnmount(self):
        print('will unmount')

    def shouldComponentUpdate(self, nextProps, nextState):
        print('should update', nextProps, nextState)
        return True

    def getSnapshotBeforeUpdate(self, prevProps, prevState):
        print('snapshot before update', prevProps, prevState)

    def componentDidUpdate(self, prevProps, prevState, snapshot):
        print('did update', prevProps, prevState, snapshot)

    def on_counter(self):
        self.setState({'counter': self.state['counter'] + 1})

    def render(self):
        print('render', self.props, self.state)
        if self.state is None:
            return None
        return self.sprite(
            ('div', {'className': 'maindiv'}, [
                ('h1', None, ['Hello ', self.props['name']]),
                ('p', None, 'Lorem ipsum dolor sit ame.'),
                ('p', {'onClick': self.on_counter}, ['Counter: ', self.state['counter']])
            ])
        )
