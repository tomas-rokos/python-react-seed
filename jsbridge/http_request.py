def send(url: str, cb, cb_data_type='json', method='GET', body=None):
    def _callback(resp):
        if cb_data_type == 'json':
            resp.json().then(lambda data: cb(data))
        elif cb_data_type == 'text':
            resp.text().then(lambda data: cb(data))
        else:
            resp.blob().then(lambda data: cb(data))
    params = {'method': method, 'body': body}
    fetch(url, params).then(_callback)
