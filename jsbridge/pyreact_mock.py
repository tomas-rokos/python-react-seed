
class Component:
    def __init__(self, props: dict = None):
        self.props = props
        self.state = None

    def sprite(self, item):
        return item

    def setState(self, val):
        self.state = val
