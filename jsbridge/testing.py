import importlib
import sys
sys.modules['jsbridge.bootstrap'] = importlib.import_module('jsbridge.bootstrap_mock')
sys.modules['jsbridge.navigation'] = importlib.import_module('jsbridge.navigation_mock')
sys.modules['jsbridge.pyreact'] = importlib.import_module('jsbridge.pyreact_mock')
