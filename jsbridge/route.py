from jsbridge.navigation import navigate
from jsbridge.pyreact import Component


class Route(Component):
    def __init__(self, props):
        super().__init__(props)

    def _on_click(self, handler):
        navigate(self.props['href'])
        handler.stopPropagation()
        handler.preventDefault()
        return False

    def render(self):
        return self.sprite(('a',
                            {'href': self.props['href'],
                             'onClick': self._on_click},
                            self.props.children))
