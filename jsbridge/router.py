from jsbridge.navigation import on_navigation, get_location
from jsbridge.pyreact import Component


class Router(Component):
    def __init__(self, props):
        super().__init__(props)
        self.custom_props = props
        on_navigation(self._on_navigation_change)
        self.state = {'path': get_location().pathname}

    def _on_navigation_change(self, loc):
        self.setState({'path': loc.pathname})

    def render(self):
        match = self.props[self.state['path']]
        backup = self.props['']
        return self.sprite((match if match else backup, None))
