def bootstrap(app):
    # Render the component in a 'container' div
    def main():
        # using React.createElement directly
        ReactDOM.render(
            React.createElement(app),
            document.getElementById('container')
        )

    # run main() when the document is ready
    document.addEventListener("DOMContentLoaded", main)
