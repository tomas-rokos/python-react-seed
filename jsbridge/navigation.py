
cbs = []


def _notify():
    new_loc = get_location()
    for cb in cbs:
        cb(new_loc)


def get_location():
    return window.location


def navigate(href: str):
    window.history.pushState(None, None, href)
    _notify()


def on_navigation(cb):
    cbs.append(cb)


window.onpopstate = _notify
