def pythonize_json(input):
    if isinstance(input, list):
        return [pythonize_json(item) for item in input]
    if str(type(input)) == '{}':
        dicted = dict(input)
        return {key: pythonize_json(dicted[key]) for key in dicted.keys()}
    return input
