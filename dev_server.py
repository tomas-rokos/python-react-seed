#!/usr/bin/env python3
from livereload import Server, shell
import os, os.path, glob
from subprocess import run

WATCH = ['frontend/*.py', 'frontend/components/*.py', 'frontend/routes/*.py']
RECOMPILE = 'frontend/app.py'


def recompile(to_compile):
    def inner():
        for fn in glob.glob(to_compile):
            run('transcrypt --map --build --nomin --esv 6 {}'.format(fn),
                shell=True, check=True)

    return inner


# initial start
recompile(RECOMPILE)()

# watch .py files
server = Server()
for watch in WATCH:
    server.watch(watch, recompile(RECOMPILE))
server.serve(root='.', port=8000)
