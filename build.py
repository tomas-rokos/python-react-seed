#!/usr/bin/env python3
from subprocess import check_call

check_call(['cd', 'frontend'])
check_call(['rm', '-rf', '__taget__'])
check_call(['cd', '..'])
check_call(['transcrypt', '--esv', '6', 'frontend/app.py'])
